variable "servers76" {
    type = string
    default = "1"
}
variable "servers77" {
    type = string
    default = "1"
}
variable "servers78" {
    type = string
    default = "1"
}
variable "servers80" {
    type = string
    default = "1"
}

variable "solidserver_host" {
  type = string
  default = "sds"
}

variable "solidserver_user" {
  type = string
  default = "admin"
}

variable "solidserver_password" {
  type = string
  default = "pwd"
}

variable "solidserver_space" {
  type = string
  default = "Local"
}
