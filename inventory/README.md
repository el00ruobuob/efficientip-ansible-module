# SOLIDserver inventory for ansible

This module provides dynamic inventory for ansible based on EfficientIP Device Manager content.

## development

* use a git clone of the ansible repository
* create virtual environment and apply requirements
* source hacking/env-setup

* clone https://github.com/ansible-collections/community.general.git inside a directory ansible_collections/community/
* copy file ansible/lib/ansible/plugins/inventory/efficientip.py in plugins/inventory/
* copy file plugins/inventory/efficientip.py in plugins/inventory/
* add a simlink general -> community.general

## test

set env variable ANSIBLE_COLLECTIONS_PATHS to the directory containing ansible_collections

* efficientip.yaml is an example of how to use it, in addition to the code itself
``` ansible-inventory -i efficientip.yaml --list|graph```

* playbook.yaml can be use to test with ansible
```ansible-playbook -i efficientip.yaml --list-hosts playbook.yaml```

### build docker images
